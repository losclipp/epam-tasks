﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryMatrix;

namespace ConsoleProgram
{
	class Program
	{
		static void Main(string[] args)
		{
			int[,] mas1 = { { 45, 5, 5 }, { 35, -70, 4 }, { 8, 10, -97 } };
			int[,] mas2 = { { 35, 68, 9 }, { -1, -7, 9 }, { -13, 23, 66 } };

			Matrix matrix1 = new Matrix(mas1);
			Matrix matrix2 = new Matrix(mas2);

			Console.WriteLine("Matrix 1:");
			PrintMatrix(matrix1);
			Console.WriteLine("Matrix 2:");
			PrintMatrix(matrix2);


			Console.WriteLine("\nMultiple:");
			PrintMatrix(matrix1 * matrix2);

			Console.WriteLine("\nAdd:");
			PrintMatrix(matrix1 + matrix2);

			Console.WriteLine("\nSubstract:");
			PrintMatrix(matrix1 - matrix2);

			Console.WriteLine("\nReverse Matrix 1:");
			PrintMatrix(matrix1.Reverse());

			Console.WriteLine("\nTransponent Matrix 2:");
			PrintMatrix(matrix2.Trasnoponent());

			Console.ReadLine();
		}
		static void PrintMatrix(Matrix matrix)
		{
			for (int i = 0; i < matrix.Height; i++)
			{
				for (int j = 0; j < matrix.Width; j++)
				{
					Console.Write("{0}\t",matrix[i,j]);
				}
				Console.WriteLine();
			}
		}
	}
}
