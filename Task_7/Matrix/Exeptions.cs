﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMatrix
{
	[Serializable]
	public class DiferentMatrixSizeExeption : ApplicationException
	{
		public DiferentMatrixSizeExeption() : base() { }
		public DiferentMatrixSizeExeption(string message) : base(message) { }
		public DiferentMatrixSizeExeption(string message, Exception ex) : base(message, ex) { }

		protected DiferentMatrixSizeExeption(System.Runtime.Serialization.SerializationInfo info,
						System.Runtime.Serialization.StreamingContext contex) : base(info,contex) { }
	}
	[Serializable]
	public class MultiplicationMatrixExeption : ApplicationException
	{
		public MultiplicationMatrixExeption() : base() { }
		public MultiplicationMatrixExeption(string message) : base(message) { }
		public MultiplicationMatrixExeption(string message, Exception ex) : base(message, ex) { }

		protected MultiplicationMatrixExeption(System.Runtime.Serialization.SerializationInfo info,
						System.Runtime.Serialization.StreamingContext contex) : base(info, contex)	{ }
	}
	[Serializable]
	public class SingularMatrixExeption : ApplicationException
	{
		public SingularMatrixExeption() :base() { }
		public SingularMatrixExeption(string message) : base(message) { }
		public SingularMatrixExeption(string message, Exception ex) : base(message, ex) { }

		protected SingularMatrixExeption(System.Runtime.Serialization.SerializationInfo info,
						System.Runtime.Serialization.StreamingContext contex) : base(info, contex)	{ }
	}
}
