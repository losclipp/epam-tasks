﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMatrix
{
    public class Matrix : IComparable<Matrix>
    {
		private decimal[,] matrix;

		public Matrix(int width, int height)
		{
			matrix = new decimal[height,width];
		}
		public Matrix(decimal[,] input)
		{
			matrix = (decimal[,]) input.Clone();
		}
		public Matrix(int[,] input)
		{
			int height = input.GetLength(0);
			int width = input.GetLength(1);
			matrix = new decimal[height, width];
			for (int i = 0; i < height; i++)
				for (int j = 0; j < width; j++)
					matrix[i, j] = input[i, j];			
		}


		public int Height { get { return matrix.GetLength(0); } }
		public int Width { get { return matrix.GetLength(1); } }
		public decimal this[int row, int coll]
		{
			get
			{
				CheckAndRunIndexOutOfRangeExeption(row, coll, this.Height, this.Width);
				return matrix[row, coll];
			}
			set
			{
				CheckAndRunIndexOutOfRangeExeption(row, coll, this.Height, this.Width);
				matrix[row, coll] = value;
			}
		}
		private void CheckAndRunIndexOutOfRangeExeption(int row, int coll, int height, int width)
		{
			if (row < 0 && coll > 0 &&
				row >= height - 1 &&
				coll >= width - 1)
				throw new IndexOutOfRangeException(
					string.Format("Indexes i = {0} j = {1}, in matrix {2}*{3}",
					row, coll, width, height));
		}


		public int CompareTo(Matrix other)
		{
			if ((this.Width * this.Height) > (other.Width * other.Height))
				return 1;
			if ((this.Width * this.Height) < (other.Width * other.Height))
				return -1;
			return 0;
		}
		public override bool Equals(object obj)
		{
			var item = obj as Matrix;

			if (item == null)
				return false;

			return this.Equals(item);

		}
		public bool Equals(Matrix other)
		{
			if (this.Width != other.Width && this.Height != other.Height)
				return false;
			for (int i = 0; i < Height; i++)
				for (int j = 0; j < Width; j++)
					if (this[i, j] != other[i, j])
						return false;
			return true;
		}

		public decimal Definer()
		{
			if (IsDefinerZero(matrix))
				return 0;
			return Definer(matrix);
		}
		private bool IsDefinerZero(decimal[,] array)
		{
			if (matrix.GetLength(0) == 0 || matrix.GetLength(1) == 0)
				return true;
			if (matrix.GetLength(0) != matrix.GetLength(1))
				return true;
			return false;
		}
		private decimal Definer(decimal[,] array)
		{
			if (array.Length == 1)
				return array[0, 0];

			decimal result = 0;
			for (int i = 0; i < array.GetLength(0); i++)
				result += array[i, 0] * CoFactor(array, i, 0);
			return result;
		}
		private decimal CoFactor(decimal[,] array, int i, int j)
		{
			return EvenOrOddPow(i, j) * Minor(array, i, j);
		}
		private int EvenOrOddPow(int i, int j)
		{
			if ((i + j) % 2 == 0)	return 1;
			return -1;
		}
		private decimal Minor(decimal[,] array, int i, int j)
		{
			decimal[,] newArray = RemoveRowAndCollumn(array, i, j);
			return Definer(newArray);
		}
		private decimal[,] RemoveRowAndCollumn(decimal[,] array, int row, int collumn)
		{
			CheckAndRunIndexOutOfRangeExeption(row, collumn, array.GetLength(0), array.GetLength(1)); 
			
			int height = array.GetLength(0) - 1;
			int width = array.GetLength(1) - 1;

			decimal[,] result = new decimal[height, width];
			int currentRow = 0, currentColl = 0;

			for (int i = 0; i < array.GetLength(0); i++)
			{
				if (i == row)	continue;

				currentColl = 0;
				for (int j = 0; j < array.GetLength(1); j++)
				{
					if (j == collumn)	continue;
					result[currentRow, currentColl] = array[i, j];
					currentColl++;
				}
				currentRow++;
			}

			return result;
		}
		


		public static Matrix Multiply(Matrix matrixOperand, decimal numberOperand)
		{
			return  new Matrix( Multiply(matrixOperand.matrix, numberOperand));
		}
		private static decimal[,] Multiply(decimal[,] matrixOperand, decimal numberOperand)
		{
			int width = matrixOperand.GetLength(1);
			int height = matrixOperand.GetLength(0);
			decimal[,] result = new decimal[height, width];

			for (int i = 0; i < height; i++)
				for (int j = 0; j < width; j++)
					result[i, j] = matrixOperand[i, j] * numberOperand;
			return result;
		}


		public Matrix Trasnoponent()
		{

			return new Matrix( Trasnoponent(this.matrix));
		}
		private decimal[,] Trasnoponent(decimal[,] array)
		{
			decimal[,] result = new decimal[array.GetLength(0), array.GetLength(1)];
			for (int i = 0; i < array.GetLength(0); i++)
				for (int j = 0; j < array.GetLength(1); j++)
					result[j, i] = array[i, j];
			return result;
		}


		public Matrix Reverse()
		{
			decimal definer = this.Definer();
			if (definer == 0)
				throw new SingularMatrixExeption();
			decimal[,] coFactor = СoFactorMatrix();
			decimal[,] transponent = Trasnoponent(coFactor);
			Matrix result = new Matrix(Multiply(transponent, definer));

			return result;
		}
		private decimal[,] СoFactorMatrix()
		{
			return this.СoFactorMatrix(this.matrix);
		}
		private decimal[,] СoFactorMatrix(decimal[,] array)
		{
			decimal[,] result = new decimal[array.GetLength(0), array.GetLength(1)];
			for (int i = 0; i < result.GetLength(0); i++)
				for (int j = 0; j < result.GetLength(1); j++)
					result[i, j] = CoFactor(array, i, j);
			return result;
		}


		public static Matrix operator * (Matrix operand1, Matrix operand2)
		{
			CheckOperand(operand1);
			CheckOperand(operand2);

			if (operand1.Width != operand2.Height)
				throw new MultiplicationMatrixExeption();

			decimal[,] result = new decimal[operand1.Height, operand2.Width];
			for (int i = 0; i < operand1.Height; i++)
			{
				for (int j = 0; j < operand2.Width; j++)
				{
					for (int k = 0; k < operand2.Height; k++)
					{
						result[i, j] += operand1[i, k] * operand2[k, j];
					}
				}
			}

			return new Matrix( result);
		}
		private static void CheckOperand(Matrix operand)
		{
			if (operand == null)
				throw new NullReferenceException();
		}
		public static Matrix operator - (Matrix operand1, Matrix operand2)
		{
			CheckOperand(operand1);
			CheckOperand(operand2);
			return Matrix.CommonOperation(operand1, operand2, (x, y) => x - y);
		}
		public static Matrix operator + (Matrix operand1, Matrix operand2)
		{
			CheckOperand(operand1);
			CheckOperand(operand2);
			return Matrix.CommonOperation(operand1, operand2, (x, y) => x + y);
		}
		private static Matrix CommonOperation(Matrix operand1, Matrix operand2, Func<decimal, decimal, decimal> operation)
		{
			if (operand1.Height != operand2.Height &&
				operand1.Width != operand2.Width)
				throw new MultiplicationMatrixExeption("Matrixes have different sizes");

			decimal[,] result = new decimal[operand1.Height, operand1.Width];
			for (int i = 0; i < operand1.Height; i++)
				for (int j = 0; j < operand1.Width; j++)
					result[i,j] = operation(operand1[i, j], operand2[i, j]);

			return new Matrix(result);
		}
	}
}
