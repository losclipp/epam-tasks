﻿using System;
using NUnit.Framework;
using LibraryMatrix;
using System.Collections.Generic;

namespace TestLibraryMatrix
{
	[TestFixture]
	public class TestMatrix
	{
		static public IEnumerable<TestCaseData> MultipleData1()
		{
			yield return new TestCaseData(new int[,] { { 1, -23 }, { 45, 3 }, { -7, 0 } },
										new int[,] { { 13 }, { -9 } },
										new int[,] { { 220 }, { 558 }, { -91 } });

		}
		static public IEnumerable<TestCaseData> MultipleData2()
		{
			yield return new TestCaseData(new int[,] { { 35, -8, 10 }, { 51, 3, 0 } },
										new int[,] { { 56, 98, 78 }, { 12, 3, -9 }, { -5, 8, -13 } },
										new int[,] { { 1814, 3486, 2672 }, { 2892, 5007, 3951 } });
		}
		[TestCaseSource("MultipleData1")]
		[TestCaseSource("MultipleData2")]
		public void MultiplyOperator(int[,] operand1, int[,] operand2, int[,] result)
		{
			Matrix first = new Matrix(operand1);
			Matrix second = new Matrix(operand2);
			Matrix third = new Matrix(result);
			
			Assert.AreEqual( third, first * second );
		}

		//---------------------------------------------------------------------------------------------------------------
	
		static public IEnumerable<TestCaseData> AddAndSubstractData1()
		{
			yield return new TestCaseData(	new int[,] { { -2, 10, 35 }, { 45, 0, 5 } },
											new int[,] { { 7, 6, -11 }, { 22, -33, 44 } },
											new int[,] { { 5, 16, 24 }, { 67, -33, 49 } },
											new int[,] { { -9, 4, 46 }, { 23, 33, -39 } }
											);

		}
		static public IEnumerable<TestCaseData> AddAndSubstractData2()
		{
			yield return new TestCaseData(	new int[,] { { 5, -10 }, { 30, 40 } },
											new int[,] { { 99, 11 }, { 65, -28 } },
											new int[,] { { 104, 1 }, { 95, 12 } },
											new int[,] { { -94, -21 }, { -35, 68 } }
											);

		}
		[TestCaseSource("AddAndSubstractData1")]
		[TestCaseSource("AddAndSubstractData2")]
		public void AddAndSubstractOperator(int[,] operand1, int[,] operand2, int[,] addResult, int[,]SubResult)
		{
			Matrix first = new Matrix(operand1);
			Matrix second = new Matrix(operand2);

			Matrix add = new Matrix(addResult);
			Matrix sub = new Matrix(SubResult);

			Assert.AreEqual(first + second, add);
			Assert.AreEqual(first - second, sub);
		}
		//---------------------------------------------------------------------------------------------------------------
		static public IEnumerable<TestCaseData> DefinerData1()
		{
			yield return new TestCaseData(new int[,] {	{ 65, -10, -7 },
														{ 31, 5, 2 }, 
														{ 0, 32, 12 } },
											-3484);
		}
		static public IEnumerable<TestCaseData> DefinerData2()
		{
			yield return new TestCaseData(new int[,]	{ { 35, 2, -5, 46 }, 
														{ -8, 21, 10, 0 }, 
														{ 32, 68, 10, 8 }, 
														{ 11, -9, -12, 3 } },
											-257746);
		}
		[TestCaseSource("DefinerData1")]
		[TestCaseSource("DefinerData2")]
		public void DefinerTest(int[,] input, int result)
		{
			Matrix matrix = new Matrix(input);
			Assert.AreEqual(matrix.Definer(), (decimal)result);
		}
		//---------------------------------------------------------------------------------------------------------------

		static public IEnumerable<TestCaseData> HeightAndWidthData1()
		{
			yield return new TestCaseData(new int[,]    { { 35, 2, -5, 46 },
														{ -8, 21, 10, 0 },
														{ 32, 68, 10, 8 },
														{ 11, -9, -12, 3 },
														{ 10, -98, -3, 3 },
														{ 2, 65, -15, 39 }},
											6, 4);
		}
		static public IEnumerable<TestCaseData> HeightAndWidthData2()
		{
			yield return new TestCaseData(new int[,]    { { 35, 2, -5, 46 },
														{ -8, 21, 10, 0 },
														{ 32, 68, 10, 8 } },													
											3, 4);
		}

		[TestCaseSource("HeightAndWidthData1")]
		[TestCaseSource("HeightAndWidthData2")]
		public void HeightWidthTest(int[,] input, int height, int width )
		{
			Matrix matrix = new Matrix(input);
			Assert.AreEqual(matrix.Height, height);
			Assert.AreEqual(matrix.Width, width);
		}
		//---------------------------------------------------------------------------------------------------------------

		static public IEnumerable<TestCaseData> IndexData1()
		{
			yield return new TestCaseData(new int[,]    { { 35, 2, -5, 46 },
														{ -8, 21, 10, 0 },
														{ 32, 68, 10, 8 } },
											1, 2, 10);
		}
		static public IEnumerable<TestCaseData> IndexData2()
		{
			yield return new TestCaseData(new int[,]    { { 35, 2, -5, 46 },
														{ -8, 21, 10, 0 },
														{ 32, 68, 10, 8 } },
											2, 3, 8);
		}
		[TestCaseSource("IndexData1")]
		[TestCaseSource("IndexData2")]
		public void IndexerTest(int[,] input, int row, int coll, int result)
		{
			Matrix matrix = new Matrix(input);
			Assert.AreEqual(matrix[row, coll], result);
		}
		//---------------------------------------------------------------------------------------------------------------

		static public IEnumerable<TestCaseData> ReverseData1()
		{
			yield return new TestCaseData(new int[,]   { { 7, 4 },
														 { 5, 3 } }	);
		}
		static public IEnumerable<TestCaseData> ReverseData2()
		{
			yield return new TestCaseData(new int[,] { { 2, 5, 7 }, { 6, 3, 4 }, { 5, -2, -3 } } );
		}

		[TestCaseSource("ReverseData1")]
		[TestCaseSource("ReverseData2")]
		public void ReverseMatrix(int[,] input)
		{
			Matrix matrix = new Matrix(input);

			int[,] array = new int[matrix.Height, matrix.Width];
			for (int i = 0; i < array.GetLength(0); i++)
				for (int j = 0; j < array.GetLength(1); j++)
					if (i == j)
						array[i, j] = 1;

			Matrix result = new Matrix(array);

			Assert.AreEqual(matrix * matrix.Reverse(), result);
			

		}
	}
}
