﻿using System;
using ConvertLibrary;

namespace ConsoleConvertor
{
	class Program
	{
		static void Main(string[] args)
		{
			ProgramConverter[] arr = { new ProgramConverter(), new ProgramHelper() };
			Console.WriteLine(arr[0].ConvertToCSharp(""));
			Console.WriteLine(arr[0].ConvertToVB(""));
			Console.WriteLine(arr[1].ConvertToCSharp(""));
			Console.WriteLine(arr[1].ConvertToVB(""));

			Console.WriteLine( ((ProgramHelper)arr[1]).CodeCheckSyntax("ProgramHelper CSharp text", "CSharp"));
			Console.ReadLine();

		
			
		}
	}
}
