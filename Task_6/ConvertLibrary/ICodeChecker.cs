﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertLibrary
{
	interface ICodeChecker
	{
		bool CodeCheckSyntax(string text, string language);
	}
}
