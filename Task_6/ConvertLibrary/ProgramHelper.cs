﻿using System;

namespace ConvertLibrary
{
	public class ProgramHelper : ProgramConverter, ICodeChecker
	{
		public bool CodeCheckSyntax(string text, string language)
		{
			if (language == "CSharp" && text == this.ConvertToCSharp(""))
				return true;
			if (language == "VB" && text == this.ConvertToVB(""))
				return true;
			return false;
		}

		public string ConvertToCSharp(string input)
		{
			return "ProgramHelper CSharp text";
		}

		public string ConvertToVB(string input)
		{
			return "ProgramHelper Visual Basic text";
		}
	}
}
