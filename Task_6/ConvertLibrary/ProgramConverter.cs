﻿using System;


namespace ConvertLibrary
{
	public class ProgramConverter : IConvertible
	{
		public string ConvertToCSharp(string input)
		{
			return "ProgramConverter CSharp text";
		}

		public string ConvertToVB(string input)
		{
			return "ProgramConverter Visual Basic text";
		}
	}
}
