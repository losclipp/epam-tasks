﻿using System;
using System.IO;

namespace StreamDecorator
{
	public class VisualisatorOfReadAmount : StreamDecorator
	{
		public Action<int> Vusualisation;
		public VisualisatorOfReadAmount( Stream stream )
		{
			base.stream = stream;
			base.AfterRead += CalculateAndRunVisualitation;
		}
		private void CalculateAndRunVisualitation()
		{
			RunVisualitation(CalculatWhichPartRead());
		}

		private int CalculatWhichPartRead()
		{
			return (int)(base.stream.Position * 100 / base.stream.Length);
		}
		private void RunVisualitation( int persents)
		{
			if (Vusualisation != null)
				Vusualisation(persents);
		}
	}
}
