﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace StreamDecorator
{
	public class PasswordStreamDecorator : StreamDecorator
	{
		public string Password { private set; get; }
		public Func<string> AskPassword;
				
		public PasswordStreamDecorator( Stream stream, string pass )
		{
			base.stream = stream;
			this.Password = pass;
			base.BeforeRead += BeforeReadHandle;
		}
		private void BeforeReadHandle()
		{
			if (this.Password != this.RunAskPassword())
				throw new ArgumentException("Password is wrong");
		}
		private string RunAskPassword()
		{
			if (AskPassword != null)
			 	return AskPassword();
			return this.Password;
		}
	}
}
