﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace StreamDecorator
{
	
    public abstract class StreamDecorator
    {
		protected Action BeforeRead = () => { };
		protected Action AfterRead = () => { };

		protected Stream stream;
		public int Read(byte[] buffer, int offset, int count)
		{
			int result;
			BeforeRead();
			result = stream.Read(buffer, offset, count);
			AfterRead();
			return result;
		}
   }
}
