﻿using System;

using System.Windows;
using System.Windows.Controls;

using System.Windows.Input;
using Microsoft.Win32;
using System.IO;

namespace Notepad
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		
		public MainWindow()
		{
			InitializeComponent();
			UnsavedChanges = true;
			FileName = "Untitled";
			FilePath = null;
			UpdateTitle();

		}

		private bool UnsavedChanges { get; set; }
		private string FileName { get; set; }
		private string FilePath { get; set; }

		private void Menu_Handler(object sender, RoutedEventArgs e)
		{
			MenuItem item = (MenuItem)sender;
			
			switch(item.Name)
			{
				case "MenuNew":
					{
						New();
						break;
					}
				case "MenuOpen":
					{
						Open();
						break;
					}
				case "MenuSave":
					{
						Save();
						break;
					}
				case "MenuSaveAs":
					{
						SaveAs();
						break;
					}
				case "MenuExit":
					{
						Exit();
						break;
					}
			}
			UpdateTitle();
		}

		private void New()
		{
			textBoxDocument.Text = string.Empty;
			FileName = "Untitled";
			FilePath = null;
			UnsavedChanges = true;
		}
		private void Open()
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.FileName = FileName; 
			dlg.DefaultExt = ".txt"; 
			dlg.Filter = "Text documents (.txt)|*.txt";
			
			if ( dlg.ShowDialog() == true )
			{
				FileInfo info = new FileInfo(dlg.FileName);
				try
				{
					using (StreamReader reader = new StreamReader(info.FullName))
					{
						textBoxDocument.Text = reader.ReadToEnd();
					}

					FileName = info.Name;
					FilePath = info.FullName;
					UnsavedChanges = false;
				}
				catch ( FileNotFoundException )
				{
					MessageBox.Show("File Not Exist", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				}
				catch
				{
					MessageBox.Show("Can not open the file", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}
		}
		private void Save()
		{
			if (FilePath == null || FilePath == string.Empty)
				SaveAs();
			try
			{
				using (StreamWriter writer = new StreamWriter(FilePath))
				{
					writer.Write(textBoxDocument.Text);
				}
				UnsavedChanges = false;
			}
			catch
			{
				MessageBox.Show("Could not Save File", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}
		private void SaveAs()
		{
			SaveFileDialog dlg = new SaveFileDialog();
			dlg.FileName = FileName; 
			dlg.DefaultExt = ".txt"; 
			dlg.Filter = "Text documents (.txt)|*.txt";

			if (dlg.ShowDialog() == true)
			{
				FileInfo info = new FileInfo(dlg.FileName);
				try
				{
					using (StreamWriter writer = new StreamWriter(FilePath))
					{
						writer.Write(textBoxDocument.Text);
					}
					UnsavedChanges = false;
					FileName = info.Name;
					FilePath = info.FullName;
				}
				catch
				{
					MessageBox.Show("Could not Save File", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				}

			}
		}
		private void Exit()
		{
			if(UnsavedChanges)
			{
				MessageBoxResult result = MessageBox.Show("Do you want to save changes?", "Warning", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
				switch(result)
				{
					case MessageBoxResult.Yes:
						{
							Save();
							Application.Current.MainWindow.Close();
							break;
						}
					case MessageBoxResult.No:
						{
							Application.Current.MainWindow.Close();
							break;
						}
					case MessageBoxResult.Cancel:
						{
							break;
						}


				}
			}
		}
		private void UpdateTitle()
		{
			string unsaved = UnsavedChanges ? " *" : string.Empty;
			mainWindow.Title = FileName + unsaved + " Notepap";
		}

		private void textBoxDocument_KeyDown(object sender, KeyEventArgs e)
		{
			char key = (char) KeyInterop.VirtualKeyFromKey(e.Key);
			if (char.IsLetterOrDigit(key))
			{
				UnsavedChanges = true;
				UpdateTitle();
			}		
			
		}
	}
}
