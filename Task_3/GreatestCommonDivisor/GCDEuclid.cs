﻿using System;

namespace GCD
{
    public class GCDEuclid : GratestCommonDivision
	{
		public override int Calculate(int a, int b)
		{
			return (b == 0) ? a : Calculate(b, a % b);
		}
	}
}
