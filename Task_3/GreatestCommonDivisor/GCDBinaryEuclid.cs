﻿using System;

namespace GCD
{
	public class GCDBinaryEuclid : GratestCommonDivision
	{
		public override int Calculate(int a, int b)
		{
			if (a == 0) return b;
			if (b == 0) return a;
			if (a == b) return a;
			if (a == 1 || b == 1) return 1;
			if ((a % 2 == 0) && (b % 2 == 0)) return 2 * Calculate(a / 2, b / 2);
			if ((a % 2 == 0) && (b % 2 != 0)) return Calculate(a / 2, b);
			if ((a % 2 != 0) && (b % 2 == 0)) return Calculate(a, b / 2);
			else
				return Calculate( Math.Abs(b - a), a);
		}
	}
}
