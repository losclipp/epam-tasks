﻿using System;
using System.Diagnostics;

namespace GCD
{
	public abstract class GratestCommonDivision
	{
		public abstract int Calculate(int a, int b);

		public virtual int CalculateAndGetExTime(int a, int b, out long ticks)
		{
			int result;
			Stopwatch watch = new Stopwatch();

			watch.Start();
			result = Calculate(a, b);
			watch.Stop();
			ticks = watch.ElapsedTicks;

			return result;
		}
		public virtual int Calculate(int a, int b, params int[] arr)
		{
			int currentGCD = Calculate(a, b);

			if (arr != null)
				for (int i = 0; i < arr.Length; i++)
					currentGCD = Calculate(currentGCD, arr[i]);
			return currentGCD;
		}
	}
}
