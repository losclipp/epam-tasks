﻿using System;
using GCD;

namespace ConsoleGCD
{
	class Program
	{
		static void Main(string[] args)
		{
			long time;
			GratestCommonDivision euclid = new GCDBinaryEuclid();
			GratestCommonDivision binary = new GCDBinaryEuclid();
						
			Console.WriteLine("Euclid: result = {0} time = {1}", euclid.CalculateAndGetExTime(30855, 18018, out time), time);
			Console.WriteLine("Binary: result = {0} time = {1}", binary.CalculateAndGetExTime(30855, 18018, out time), time);
		}
	}
}
