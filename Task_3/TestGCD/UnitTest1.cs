﻿using System;
using NUnit.Framework;
using GCD;

namespace TestGCD
{
	[TestFixture]
	public class UnitTest1
	{

		[TestCase(0, 34, 34)]
		[TestCase(33, 0, 33)]
		[TestCase(0, 0, 0)]
		[TestCase(1, 154, 1)]
		[TestCase(654, 1, 1)]
		[TestCase(1, 1, 1)]
		[TestCase(4564, 7987, 1141)]
		[TestCase(974592, 45660, 12)]
		[TestCase(11364588, 9688048, 4)]
		[TestCase(4564, (-7987), 1141)]
		[TestCase(-974592, 45660, 12)]
		public void GCDForTwoParams(int a, int b, int result)
		{
			GratestCommonDivision euclid = new GCDEuclid();
			GratestCommonDivision binary = new GCDBinaryEuclid();

			Assert.AreEqual(result, Math.Abs( euclid.Calculate(a, b) ));
			Assert.AreEqual(result, Math.Abs( binary.Calculate(a, b) ));
			
		}
		[TestCase(45, 78, 3, 0)]
		[TestCase(605, 550, 55, 1485)]
		[TestCase(550, 605, 55, 605)]
		[TestCase(8602, -1932, 46, 5290, -49680)]
		public void GCDForSeveralParams(int a, int b, int result , params int[] c)
		{
			GratestCommonDivision euclid = new GCDEuclid();
			GratestCommonDivision binary = new GCDBinaryEuclid();

			Assert.AreEqual(result, Math.Abs( euclid.Calculate(a, b, c) ));
			Assert.AreEqual(result, Math.Abs( binary.Calculate(a, b, c) ));
		}
	}
}
