﻿using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyConverter;
using NUnit.Framework;

namespace ConvertLibraryTest
{
	[TestFixture]
	public class PointConverterTest
	{
		
		[TestCase("14.45,7.635",14.45f, 7.635f)]
		[TestCase("1445.4571,74534.5750144", 1445.4571f, 74534.5750144f)]
		[TestCase("10,18", 10f, 18f)]
		public void SetPoint_Test(string str, float X, float Y)
		{
			PointConverter point = new PointConverter();
			point.SetPoint(str);
			Assert.AreEqual(point.X, X);
			Assert.AreEqual(point.Y, Y);

		}

		
		[TestCase ("")]
		[TestCase(null)]
		public void SetPoint_NullReferenceExeptionTest(string str)
		{
			PointConverter point = new PointConverter();
			Assert.Throws<NullReferenceException>(() => point.SetPoint(str));

		}
		
		
		[TestCase ("456.sdf,46")]
		[TestCase("12.6, 45.57")]
		[TestCase("456,46.654,45")]
		[TestCase(" 7.6,56.57")]
		public void SetPoint_FormatExeptionTest(string str)
		{
			PointConverter point = new PointConverter();
			Assert.Throws<FormatException>( () => point.SetPoint(str));
		}
	}
}
