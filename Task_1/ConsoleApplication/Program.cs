﻿using System;
using MyConverter;


namespace ConsoleApplication
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.InputEncoding = System.Text.Encoding.UTF8;
			PointConverter point = new PointConverter();
			string inputLine;

			while ((inputLine = Console.ReadLine()) != null)
			{
				point.SetPoint(inputLine);
				Console.WriteLine(point.ToString());
			}
					
		}
	}
}
