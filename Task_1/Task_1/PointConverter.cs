﻿using System;
using System.Globalization;

namespace MyConverter
{
    public class PointConverter
    {
		public float X { get; private set; }
		public float Y { get; private set; }

		public PointConverter()
		{
			X = float.NaN;
			Y = float.NaN;
		}

		public void SetPoint(string str)
		{

			if (IsStringEmpty(str))
				throw new NullReferenceException("Input string is null or empty");
			else
			{
				string[] splitedStrings = str.Split(',');

				if (splitedStrings.Length != 2)
				{
					this.X = float.NaN;
					this.Y = float.NaN;
					ThrowFormatExeption();
				}
				else
				{
					this.X = FromStringToFloat(splitedStrings[0]);
					this.Y = FromStringToFloat(splitedStrings[1]);
				}
			}
		}
		private bool IsStringEmpty(string str)
		{
			if (str == null || str == string.Empty)
				return true;
			return false;
		}
		private void ThrowFormatExeption()
		{
			throw new FormatException("Exapmle of Correct format: [134.564,898.123]");
		}
		private float FromStringToFloat(string str)
		{
			NumberStyles style = NumberStyles.AllowDecimalPoint;
			CultureInfo culture = CultureInfo.CreateSpecificCulture("en-GB");

			float outParam;
			if (!float.TryParse(str, style, culture, out outParam))
			{
				outParam = float.NaN;
				ThrowFormatExeption();
			}
			return outParam;
		}
		
				
		public override string ToString()
		{
			return "X: " + X.ToString() + " Y: " + Y.ToString();
		}
    }
}
