﻿using System;
using MathCollections;

namespace ConsoleProgram
{
	class Program
	{
		static void Main(string[] args)
		{

			Console.WriteLine("Vector class");
			Vector vector1 = new Vector(12.3, 4.5, 1.2);
			Vector vector2 = new Vector(1.4, -5.5);

			Console.WriteLine("vector 1: {0}", vector1.ToString());
			Console.WriteLine("vector 2: {0}", vector2.ToString());
			Console.WriteLine("vector 1 Length: {0}", vector1.Length);
			Console.WriteLine("vector 2 Length: {0}", vector2.Length);

			Console.WriteLine("equals {0}", Vector.Equals(vector1, vector2));
			Console.WriteLine("Operator + : {0}", (vector1 + vector2).ToString());
			Console.WriteLine("Operator - : {0}", (vector1 - vector2).ToString());
			Console.WriteLine("Operator * : {0}", (vector1 * vector2).ToString());
			Console.WriteLine("Operator ^ : {0}", (vector1 ^ vector2).ToString());

			Console.WriteLine("\nPolynomial class");
			Polynomial polinom1 = new Polynomial(new int[] { 1, 4, 4 });
			Polynomial polinom2 = new Polynomial(new int[] { 1, 10, 4 });

			Console.WriteLine("polinom 1: {0}", (string)polinom1);
			Console.WriteLine("polinom 2: {0}", (string)polinom2);

			Console.WriteLine("polinom 1 degree: {0}", polinom1.Degree);
			Console.WriteLine("polinom 2 degree: {0}", polinom2.Degree);

			Console.WriteLine("Operator > : {0}", polinom1 > polinom2);
			Console.WriteLine("Operator < : {0}", polinom1 < polinom2);
			Console.WriteLine("Operator == : {0}", polinom1 == polinom2);
			Console.WriteLine("Operator != : {0}", polinom1 != polinom2);

			Console.ReadLine();
		}
	}
}
