﻿using System;
using NUnit.Framework;
using MathCollections;

namespace TestMathCollections
{
	[TestFixture]
	public class TestPolynomial
	{
		private int[] array1 = { 12, 45, -89, 65, 2, -8 };
		static private int[] array2 = { 12, 45, -89, 65, 2 };
		static private int[] array3 = { 12, 45, -89, 1, 2, -8 };

		[TestCase(2, 0)]
		[TestCase(0, 54)]
		[TestCase(7, 12)]
		[TestCase(4, 789)]
		public void IndexerTest(int index, int result)
		{
			int[] coef = { 54, 87, 0, 45, 789, 64, 98, 12, 33 };
			Polynomial polynom = new Polynomial(coef);
			Assert.AreEqual(polynom[index], result);
		}
		[TestCase(5, new int[] { 12, 4, 67, 45, 68, 10 } )]
		[TestCase(3, new int[] { 12, 4, 67, 45 } )]
		public void DegreeTest(int result, int[] input)
		{
			Polynomial polynom = new Polynomial(input);
			Assert.AreEqual(polynom.Degree, result);
		}
		[TestCase( 1181, 3, new int[] { 5, 8, 2, 9, 11} ) ]
		[TestCase( 1226, 7, new int[] { 1, 14, 23 })]
		public void CalculateTest(long result, int x, int[] coef)
		{
			Polynomial polynom = new Polynomial(coef);
			Assert.AreEqual( polynom.CalculateResult(x), result );
		}
		//[Test]
		[TestCase(	"5*x^0 + 7*x^1 + 95*x^2 + 10*x^3",
					new int[] { 5, 7, 95, 10} )]
		[TestCase(	"0*x^0 + 15*x^1 + 35*x^2 + 65*x^3 + 0*x^4 + 1*x^5",
					new int[] { 0, 15, 35, 65, 0, 1 })]
		public void StringOperatorTest(string result, int[] coef)
		{
			Polynomial polynom = new Polynomial(coef);
			string str = (string)polynom;
			Assert.AreEqual( (string)polynom, result);
		}

		//[Test]
		[TestCase(	new int[] { 12, 45, -89, 65, 2, -8 },
					new int[] { 12, 45, -89, 65, 2, -8 },
					true)]
		[TestCase(	new int[] { 12, 45, -89, 65, 2, -8 },
					new int[] { 12, 45, -89, 65, -8 },
					false)]
		[TestCase(	new int[] { 12, 45, -89, 65, 2, -8 },
					new int[] { 12, 45, -89, 11, 2, -8 },
					false)]
		public void OperatorSameAndNotSame(int [] coef1, int[] coef2, bool isSame)
		{
			Polynomial operand1 = new Polynomial(coef1);
			Polynomial operand2 = new Polynomial(coef2);

			Assert.AreEqual(operand1 == operand2, isSame);
			Assert.AreEqual(operand1 != operand2, !isSame);
		}


		//[Test]
		[TestCase(	new int[] { 12, 45, -89, 65, 2, -8 },
					new int[] { 12, 45, -89, 65, -8 },
					true)]
		[TestCase(	new int[] { 12, 45, -89, 8, 2, -8 },
					new int[] { 12, 45, -89, 11, 2, -8 },
					false)]
		public void OperatorMoreAndLess(int[] coef1, int[] coef2, bool isMore)
		{
			Polynomial operand1 = new Polynomial(coef1);
			Polynomial operand2 = new Polynomial(coef2);

			Assert.AreEqual(operand1 > operand2, isMore);
			Assert.AreEqual(operand1 < operand2, !isMore);

		}
	}
}
