﻿using System;
using NUnit.Framework;
using MathCollections;

namespace TestMathCollections
{
	[TestFixture]
	public class TestVector
	{
		[TestCase(3.4, 71.2, 5.1, 5.1, 2)]
		[TestCase(3.4, 71.2, 5.1, 71.2, 1)]
		[TestCase(3.4, 71.2, 5.1, 3.4, 0)]
		public void Index(double x, double y, double z, double result, int index)
		{
			Vector vector = new Vector(x, y, z);
			Assert.AreEqual(result, vector[index]);
		}

		[TestCase(5)]
		[TestCase(-1)]
		public void Index_Exeption( int index )
		{
			Vector vector = new Vector(1, 3, 85);
			Assert.Throws<IndexOutOfRangeException>( () => { var v = vector[index]; });
		}
		[TestCase(2, 4, 4, 6)]
		[TestCase(3, -4, 0, 5)]
		public void Length(double x, double y, double z, double result)
		{
			Vector vector = new Vector(x, y, z);
			Assert.AreEqual(result, vector.Length);
		}

		[TestCase(12.4, 5.6, 7)]
		[TestCase(0, 59, 0)]
		public void Equals(double x, double y, double z)
		{
			Vector vector1 = new Vector(x, y, z);
			Vector vector2 = new Vector(x, y, z);
			Vector vector3 = new Vector(x, y, y);

			Assert.IsTrue( Vector.Equals(vector1, vector2) );
			Assert.IsFalse( Vector.Equals(vector1, vector3) );
		}

		[TestCase(	5.5,	1.4,	-4.5,
					2.1,	4,		1.5,
					7.6,	5.4,	-3,
					3.4,	-2.6,	-6   )]
		[TestCase( -8.35,	3.3,	0.45,
				    6.7,	0.5,	8.65,
				    -1.65,	3.8,	9.1,
					-15.05,	2.8,	-8.2  )]
		public void PlusAndMinusOperators( double x1, double y1, double z1,
										   double x2, double y2, double z2,
										   double xPlus, double yPlus, double zPlus,
										   double xMinus, double yMinus, double zMinus)
		{
			Vector vector1 = new Vector(x1, y1, z1);
			Vector vector2 = new Vector(x2, y2, z2);

			Vector plusResult = new Vector(xPlus, yPlus, zPlus);
			Vector minusResult = new Vector(xMinus, yMinus, zMinus);
			
			Assert.IsTrue(Vector.Equals(vector1 + vector2, plusResult));
			Assert.IsTrue(Vector.Equals(vector1 - vector2, minusResult));

		}

		[TestCase(	45,		12.5,		78.1,
					23.8,	89.23,		48.9,
					0.887251302265,		6005.465   )]
		[TestCase(	18.34,	24.02,		13.5,
					48.5,	-52.06,		-71.01,
					1.9786190350959,	-1319.6262 )]

		public void AngleAndMultiplyOperators(	double x1, double y1, double z1,
												double x2, double y2, double z2,
												double angleResult, double multiplyReult)
		{
			Vector vector1 = new Vector(x1, y1, z1);
			Vector vector2 = new Vector(x2, y2, z2);
			
			double doubleError = 0.0000001;

			Assert.IsTrue( Math.Abs( (vector1 ^ vector2) - angleResult ) < doubleError );
			Assert.IsTrue( Math.Abs( (vector1 * vector2) - multiplyReult ) < doubleError);

		}

	}

	
}
