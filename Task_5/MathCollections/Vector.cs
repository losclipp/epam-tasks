﻿using System;

namespace MathCollections
{
    public class Vector
    {
		protected double X;
		protected double Y;
		protected double Z;

		public Vector( double x, double y ) : this(x, y, 0) { }
		public Vector( double x, double y, double z )
		{
			this.X = x;
			this.Y = y;
			this.Z = z;
		}
		
		public double this[int index]
		{
			get
			{
				switch(index)
				{
					case 0:		return X;
					case 1:		return Y;
					case 2:		return Z;
					default:	throw new IndexOutOfRangeException("Index must be 0 or 1 or 2");
						
				}				
			}
		}
		public double Length
		{
			get
			{
				return Math.Sqrt( Math.Pow(this.X, 2) + Math.Pow(this.Y, 2) + Math.Pow(this.Z, 2) );
			}
		}

		public static bool Equals(Vector vector1, Vector vector2)
		{
			double doubleError = 0.0000001;
			if ( Math.Abs( vector1.X - vector2.X ) < doubleError &&
				 Math.Abs( vector1.Y - vector2.Y ) < doubleError &&
				 Math.Abs( vector1.Z - vector2.Z ) < doubleError)
				return true;
			return false;
		}

		public static Vector operator + ( Vector vector1, Vector vector2)
		{
			return Vector.CommonOperation(vector1, vector2, (x, y) => x + y );
		}
		private static Vector CommonOperation(Vector v1, Vector v2, Func<double, double, double> operation)
		{
			double resultX = operation(v1.X, v2.X);
			double resultY = operation(v1.Y, v2.Y);
			double resultZ = operation(v1.Z, v2.Z);

			return new Vector(resultX, resultY, resultZ);
		}
		public static Vector operator - (Vector vector1, Vector vector2)
		{
			return Vector.CommonOperation(vector1, vector2, (x, y) => x - y);
		}

		public static double operator * (Vector vector1, Vector vector2)
		{
			return (vector1.X * vector2.X) + (vector1.Y * vector2.Y) + (vector1.Z * vector2.Z);
		}
		public static double operator ^ (Vector vector1, Vector vector2)
		{
			double cosAlpha = (vector1 * vector2) / (vector1.Length * vector2.Length);
			return Math.Acos(cosAlpha);
		}

		public override string ToString()
		{
			return string.Format("X: {0}, Y: {1}, Z:{2}", this.X, this.Y, this.Z);
		}
	}
}
