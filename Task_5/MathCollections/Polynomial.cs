﻿using System;
using System.Text;

namespace MathCollections
{
	public class Polynomial : IComparable<Polynomial>
	{
		protected int[] coefficients;

		public Polynomial( params int[] coef )
		{
			if (coef == null || coef.Length == 0)
				throw new ArgumentNullException("Input array is Null or Empty");
			coefficients = coef;
		}

		public int this[int index]
		{
			get
			{
				if (index < 0 && index >= coefficients.Length)
					throw new IndexOutOfRangeException();
				return coefficients[index];
			}
		}
		public int Degree
		{
			get { return coefficients.Length - 1; }
		}

		public long CalculateResult(int x)
		{
			long result = 0;
			for (int i = 0; i < coefficients.Length; i++)
			{
				result += (long)Math.Pow(x, i) * coefficients[i];
			}
			return result;
		}

		public int CompareTo( Polynomial obj )
		{
			if (this.Degree > obj.Degree)	return 1;
			if (this.Degree < obj.Degree)	return -1;

			for (int i = this.Degree; i >= 0; i--)
			{
				if (this[i] > obj[i])	return 1;
				if (this[i] < obj[i])	return -1;
			}

			return 0;
		}
		public static bool operator > (Polynomial operand1, Polynomial operand2)
		{
			return operand1.CompareTo(operand2) == 1;
		}
		public static bool operator < (Polynomial operand1, Polynomial operand2)
		{
			return operand1.CompareTo(operand2) == -1;
		}
		public static bool operator == (Polynomial operand1, Polynomial operand2)
		{
			return operand1.CompareTo(operand2) == 0;
		}
		public static bool operator != (Polynomial operand1, Polynomial operand2)
		{
			return operand1.CompareTo(operand2) != 0;
		}

		public static explicit operator string(Polynomial obj)
		{
			StringBuilder builder = new StringBuilder();
			for( int i = 0; i < obj.coefficients.Length; i++)
			{
				builder.AppendFormat("{0}*x^{1} + ", obj[i], i);
			}
			builder.Length -= 3;
			return builder.ToString();
		}
	}
}
