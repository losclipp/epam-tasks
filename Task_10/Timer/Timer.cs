﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace Library
{
    public class Timer
    {
		private object lockEnabled;
		private bool enabled;

		public event EventHandler<EventArgs> ElapsedEvent = delegate { };
		public bool Enabled
		{
			get
			{
				lock(lockEnabled)
				{
					return enabled;
				}
			}
			set
			{
				lock(lockEnabled)
				{
					if (value && !enabled)
						StartRunner();
					else
						enabled = value;			
				}
			}
		}
		public int Interval { get; set; }


		public Timer()
		{
			lockEnabled = new object();
			enabled = false;
			Interval = 1;
		}
		public Timer(int seconds) : this()
		{
			Interval = seconds;
		}
				

		private void StartRunner()
		{
			enabled = true;
			Thread thread = new Thread(new ThreadStart(Runner));
			thread.Start();
		}	
		private void Runner()
		{
			while (enabled)
			{
				ElapsedEvent(this, null);
				Thread.Sleep(Interval * 1000);
			}
		}
		
    }
}
