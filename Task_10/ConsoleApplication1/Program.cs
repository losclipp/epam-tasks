﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library;

namespace ConsoleApplication1
{
	class Program
	{
		static void Main(string[] args)
		{
			ClassA a = new ClassA();
			ClassB b= new ClassB();

			Timer timer = new Timer();
			timer.Interval = 2;
			timer.ElapsedEvent += delegate { a.Print(); };
			timer.ElapsedEvent += (x, y) => { b.Print(); };
			timer.Enabled = true;

			Console.ReadLine();
			timer.Enabled = false;

			Console.ReadLine();
			timer.Interval = 5;
			timer.Enabled = true;

			Console.ReadLine();
			timer.Enabled = true;

			Console.ReadLine();
		}
	}


}
