﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
	class ClassA
	{
		public void Print()
		{
			Console.WriteLine("This message is from A");
		} 
	}
	class ClassB
	{
		public void Print()
		{
			Console.WriteLine("And this message is from B");
		}
	}
}
