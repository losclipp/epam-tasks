﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Part_2;

namespace ConsoleFactory
{
	class Program
	{
		static void Main(string[] args)
		{
			Human human1 = new Human(new Male());
			Human human2 = new Human(new Female());

			Console.WriteLine("Human 1:");
			Console.WriteLine("Clothes: {0}", human1.ClothesMethod());
			Console.WriteLine("Accessory: {0}", human1.AccesoryMethod());

			Console.WriteLine("\nHuman 2:");
			Console.WriteLine("Clothes: {0}", human2.ClothesMethod());
			Console.WriteLine("Accessory: {0}", human2.AccesoryMethod());

			Console.ReadLine();

		}
	}
}
