﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part_2
{
	public class Human
	{
		IAccessory accesory;
		IClothes clothes;

		public Human( IFactory factory)
		{
			accesory = factory.CreateAccesory();
			clothes = factory.CreateClotes();
		}
		public string AccesoryMethod() { return accesory.GetType(); }
		public string ClothesMethod() { return clothes.GetType(); }
	}
}
