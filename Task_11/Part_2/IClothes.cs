﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part_2
{

	public interface IClothes
	{
		string GetType();
	}
	public class Dress : IClothes
	{
		string IClothes.GetType() { return "Dress"; }
	}
	public class Shirt : IClothes
	{
		string IClothes.GetType() { return "Shirt"; }		
	}

	
}
