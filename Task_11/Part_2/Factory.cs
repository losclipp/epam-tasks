﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part_2
{
    public interface IFactory
	{
		IClothes CreateClotes();
		IAccessory CreateAccesory();
    }
	public class Male : IFactory
	{
		public IAccessory CreateAccesory() { return new Glasses(); }
		public IClothes CreateClotes() { return new Shirt(); }

	}
	public class Female : IFactory
	{
		public IAccessory CreateAccesory() { return new Earnrings(); }
		public IClothes CreateClotes() { return new Dress(); }
	}


}
