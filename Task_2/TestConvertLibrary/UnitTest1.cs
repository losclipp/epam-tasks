﻿using System;
using NUnit.Framework;
using Converter;

namespace TestConvertLibrary
{
	[TestFixture]
	public class UnitTest1
	{
		[TestCase(0, ExpectedResult ="0")]
		[TestCase(98, ExpectedResult = "1100010")]
		[TestCase(13, ExpectedResult = "1101")]
		public string ToBinaryStringMethod(int val)
		{
			return MyConverter.ToBinaryString(val);
		}
		[TestCase (-4)]
		public void ArithmeticExceptionTest(int val)
		{
			Assert.Throws<ArithmeticException>( () => MyConverter.ToBinaryString(val) );
		}
	}
}
