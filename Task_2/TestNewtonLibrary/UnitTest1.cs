﻿using System;
using NUnit.Framework;

namespace TestNewtonLibrary
{
	[TestFixture]
	public class NewtonTest
	{
		[TestCase(6, 39, 0.01)]
		[TestCase(2, 4, 1)]
		[TestCase(8, 5646, 0.01)]
		public void SqrtMethod(double n, double A, double accuracy)
		{
			double newtonResult = Newton.Newton.sqrt(n, A, accuracy);
			double standartResult = Math.Pow(A, 1/n);

			Assert.True((accuracy >= Math.Abs(newtonResult - standartResult)));
		}
		[TestCase(double.NaN, 39, 0.01)]
		[TestCase(6, double.PositiveInfinity, 0.01)]
		[TestCase(6, 39, double.NegativeInfinity)]
		public void NotFiniteNumberExceptionSqrt(double n, double A, double accuracy)
		{
			Assert.Throws<NotFiniteNumberException>( () => Newton.Newton.sqrt( n, A, accuracy ));
		}
		[TestCase(6, 39, -0.01)]
		[TestCase(6, -39, 0.01)]
		public void ArithmeticException_Sqrt(double n, double A, double accuracy)
		{
			Assert.Throws<ArithmeticException>( () => Newton.Newton.sqrt(n, A, accuracy) );
		}
	}
}
