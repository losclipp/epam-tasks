﻿using System;
using Converter;

namespace ConsoleConverter
{
	class Program
	{
		static void Main(string[] args)
		{
			int value = 65;
			Console.WriteLine("{0} - {1}", value, MyConverter.ToBinaryString(value));
			value = 41;
			Console.WriteLine("{0} - {1}", value, MyConverter.ToBinaryString(value));
		}
	}
}
