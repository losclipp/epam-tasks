﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Converter
{
    public static class MyConverter
    {
		public static string ToBinaryString(int val)
		{
			if (val < 0)
				throw new ArithmeticException("Parametr must be greater than zero");
			return Convert.ToString(val, 2);
		}
    }
}
