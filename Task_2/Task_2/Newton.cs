﻿using System;

namespace Newton
{
    public static class Newton
    {
		public static double sqrt(double n, double A, double accuracy)
		{

			checkParametr(n);
			checkParametr(A);
			checkParametr(accuracy);

			if (A < 0 && (n%2 == 0))
				throw new ArithmeticException("The second parameter must be greater than zero");
			
			if( accuracy < 0)
				throw new ArithmeticException("The Third parameter must be greater than zero");

			double x0 = 1;
			double x1 = (1 / n) * ((n - 1) * x0 + A / Math.Pow(x0, n - 1));

			while( Math.Abs(x0 - x1) > accuracy )
			{
				x0 = x1;
				x1 = (1 / n) * ((n - 1) * x0 + A / Math.Pow(x0, n - 1));
			}
			return x1;
		}

		private static void checkParametr(double val)
		{
			if(double.IsNaN(val) || double.IsInfinity(val))
				throw new NotFiniteNumberException("One of the parameters has Nan or Infinity");
		}

		public static double CompareWithPow(double n, double A, double accuracy)
		{
			return sqrt(n, A, accuracy) - Math.Pow(A, 1 / n);
		}
    }
}
