﻿using System;


namespace ConsoleNewton
{
	class Program
	{
		static void Main(string[] args)
		{
			double result = Newton.Newton.sqrt(13, 8192, 0.01);
			double compareResult = Newton.Newton.CompareWithPow(13, 8192, 0.01);

			Console.WriteLine("sqrt result = {0}", result);
			Console.WriteLine("Compare result = {0}", compareResult);
		}
	}
}
