﻿using System;
using NUnit.Framework;
using Figure;

namespace UnitTestProject1
{
	[TestFixture]
	public class TriangleTest
	{
		[TestCase( 1.23, 5.36, 4.56, 9.65,
				   6.54, 7.45, 1.23, 5.35,
				   6.54, 7.45, 4.56, 9.65)]
		[TestCase( 0.23, 78.3, 94.3, 74.0, 
				   0.0, 78.3, 94.3, 74.1,
				   45.3, 78.2, 45.3, 78.2)]
		[TestCase(1.23, 8.3, 94.3, 74.0,
				   94.3, 74.0, 1.23, 8.3,
				   1.23, 8.3, 1.23, 8.3)]
		public void TriangleConstructorArgumentExeption(double Ax1, double Ay1, double Ax2, double Ay2,
														double Bx1, double By1, double Bx2, double By2,
														double Cx1, double Cy1, double Cx2, double Cy2)
		{

			Side a = new Side(new Point(Ax1, Ay1), new Point(Ax2, Ay2));
			Side b = new Side(new Point(Bx1, By1), new Point(Bx2, By2));
			Side c = new Side(new Point(Cx1, Cy1), new Point(Cx2, Cy2));
			Assert.Throws<ArgumentException>(() => { Triangle t = new Triangle(a, b, c); });
		}


		[TestCase( 1.56, 7.36, 10.3, 6.1,
				  10.3, 6.1, 12, 3.6,
				  1.56, 7.36, 12, 3.6,
				  22.95004991, 9.854)]
		[TestCase(1.56, 7.36, 10.3, 6.1,
				  12, 3.6, 10.3, 6.1,
				  1.56, 7.36, 12, 3.6,
				  22.95004991, 9.854)]
		[TestCase(1.3, 74.6, 55.5, -32.6,
				  45.1, -65.3, 55.5, -32.6,
				  45.1, -65.3, 1.3, 74.6,
				  301.03297599, 1443.61)]
		public void TrianglePerimetrAndSquare(double Ax1, double Ay1, double Ax2, double Ay2,
											  double Bx1, double By1, double Bx2, double By2,
											  double Cx1, double Cy1, double Cx2, double Cy2,
											  double perimeter, double square)
		{
			Side a = new Side(new Point(Ax1, Ay1), new Point(Ax2, Ay2));
			Side b = new Side(new Point(Bx1, By1), new Point(Bx2, By2));
			Side c = new Side(new Point(Cx1, Cy1), new Point(Cx2, Cy2));

			Triangle triangle = new Triangle(a, b, c);

			Assert.IsTrue(Math.Abs( triangle.Perimeter() - perimeter) < 0.00001);
			Assert.IsTrue(Math.Abs(triangle.Square() - square) < 0.00001);
		}
	}
}
