﻿using System;
using Figure;

namespace TriangleConsole
{
	class Program
	{
		static void Main(string[] args)
		{
			Side a = new Side(new Point(4.5, 3.5), new Point(7.1, 6.7));
			Side b = new Side(new Point(7.1, 6.7), new Point(10, 0));
			Side c = new Side(new Point(4.5, 3.5), new Point(10, 0));

			Triangle triangle = new Triangle(a, b, c);
			Console.WriteLine("Perimetr = {0}\nSquare = {1}",triangle.Perimeter(), triangle.Square());

			a = new Side(new Point(4.5, 3.5), new Point(4.5, 3.5));
			b = new Side(new Point(7.1, 6.7), new Point(4.5, 3.5));
			c = new Side(new Point(4.5, 3.5), new Point(7.1, 6.7));
			try
			{
				triangle = new Triangle(a, b, c);
			}
			catch (ArgumentException ex)
			{
				Console.WriteLine(ex.Message);
			}			
		}
	}
}
