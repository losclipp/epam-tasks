﻿using System;

namespace Figure
{
	public class Triangle
    {
		public Side a { get; private set; }
		public Side b { get; private set; }
		public Side c { get; private set; }

		public Triangle(Side a, Side b, Side c)
		{
			this.a = a;
			this.b = b;
			this.c = c;

			CheckValuesOfSides();
			CheckThatSidesBuildTriangle();
		}

		private void CheckValuesOfSides()
		{
			CheckSideValue(a);
			CheckSideValue(b);
			CheckSideValue(c);
		}
		private void CheckSideValue(Side s)
		{
			CheckPointValue(s.V1);
			CheckPointValue(s.V2);
		}
		private void CheckPointValue(Point p)
		{
			if (double.IsInfinity(p.X) || double.IsInfinity(p.Y)
				  || double.IsNaN(p.X) || double.IsNaN(p.Y))
				throw new NotFiniteNumberException("One of the Points coordinate has Nan or Infinity");
		}

		private void CheckThatSidesBuildTriangle()
		{
			Point V1, V2, V3;

			if (!HasCommonPoint(a, b, out V1))
				throw new ArgumentException("Side a and b has no common point");
			if (!HasCommonPoint(a, c, out V2))
				throw new ArgumentException("Side a and c has no common point");
			if (!HasCommonPoint(b, c, out V3))
				throw new ArgumentException("Side b and c has no common point");

			if (AreOnSameLine(V1,V2,V3))
				throw new ArgumentException("Vertexes are on the same Line");
		}
		private bool HasCommonPoint(Side a, Side b, out Point p)
		{
			p = new Point { X = 0, Y = 0 };
			if (a.V1.Equals(b.V1) || a.V1.Equals(b.V2)) { p = a.V1; return true; }
			if (a.V2.Equals(b.V1) || a.V2.Equals(b.V2)) { p = a.V2; return true; }
			return false;
		}
		private bool AreOnSameLine(Point v1, Point v2, Point v3)
		{
			// ax + b = y
			double a, b;
			double x1, x2, x3, y1, y2, y3;
			x1 = v1.X;	y1 = v1.Y;
			x2 = v2.X;	y2 = v2.Y;
			x3 = v3.X;	y3 = v3.Y;

			a = (y2 - y1) / (x2 - x1);
			b = (y2 * x1 - y1 * x2) / (x1 - x2);

			double doubleError = 0.0001;
			double y3Result = a * x3 + b;
			if (Math.Abs(y3 - y3Result) < doubleError || double.IsNaN(y3Result) || double.IsInfinity(y3Result))
				return true;
			return false;
		}
			
		public double Square()
		{
			double halfPerimeter = this.Perimeter() / 2;
			return Math.Sqrt(	halfPerimeter * 
								(halfPerimeter - a.Length) * 
								(halfPerimeter - b.Length) * 
								(halfPerimeter - c.Length) );
		}
		public double Perimeter()
		{
			return a.Length + b.Length + c.Length;
		}

	}
}
