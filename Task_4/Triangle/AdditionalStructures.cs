﻿using System;

namespace Figure
{
	public struct Point
	{
		public double X;
		public double Y;

		public Point( double x, double y)
		{
			X = x;
			Y = y;
		}
	}
	public struct Side
	{
		public Point V1;
		public Point V2;

		public Side(Point v1, Point v2)
		{
			V1 = v1;
			V2 = v2;
		}

		public double Length
		{
			get { return Math.Sqrt(Math.Pow(V1.X - V2.X, 2) + Math.Pow(V1.Y - V2.Y, 2)); }
		}

	}
}
